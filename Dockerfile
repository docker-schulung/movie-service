FROM maven AS builder

WORKDIR /home

ADD . .
RUN mvn clean package -D skipTests

#--------------------

FROM openjdk:17-alpine

WORKDIR /home

COPY --from=builder /home/target/movie-service-0.0.1-SNAPSHOT.jar ./app.jar

CMD ["java", "-jar", "app.jar"]
